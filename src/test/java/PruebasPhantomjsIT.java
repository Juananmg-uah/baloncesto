import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.TimeUnit;


class PruebasPhantomjsIT {
  private static WebDriver driver=null;

  @Test
  void tituloIndexTest() {
    DesiredCapabilities caps = new DesiredCapabilities();

    caps.setJavascriptEnabled(true);
    caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
    caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});

    driver = new PhantomJSDriver(caps);

    driver.navigate().to("http://localhost:8080/Baloncesto/");

    assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");
    System.out.println(driver.getTitle());
    driver.close();
    driver.quit();
  }


  @Test
  void votosACeroTest() {
    DesiredCapabilities caps = new DesiredCapabilities();

    caps.setJavascriptEnabled(true);
    caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
    caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});

    driver = new PhantomJSDriver(caps);

    driver.navigate().to("http://localhost:8080/Baloncesto/");


    /* 1. Pulsación: "Poner Votos a cero" */
    driver.findElement(By.id("votosACero")).click();
    driver.findElement(By.id("returnButton")).click();


    /* 2. Pulsación: "Ver Votos" */
    driver.findElement(By.id("verVotos")).click();


    /* 3. Comprobación de que todos los jugadores tienen 0 votos */
    WebElement tabla = driver.findElement(By.id("tablaVotos"));
    ArrayList<WebElement> rows = new ArrayList<>(tabla.findElements(By.tagName("tr")));

    ArrayList<WebElement> listaVotos = new ArrayList<>();

    for(int i = 1; i < rows.size(); i++) {
      List<WebElement> celdas = rows.get(i).findElements(By.tagName("td"));
      listaVotos.add(celdas.get(2));
    }

    boolean expResult = true;
    boolean result = true;
    int j = 0;

    // Se comprueba para cada jugador que votos == 0
    while(result && j < listaVotos.size()) {
      if(!listaVotos.get(j).getText().trim().equals("0")) {
        result = false;
      }
      j++;
    }

    assertEquals(expResult, result, "  [!] Alguno de los votos nos son 0");

    driver.close();
    driver.quit();
  }


  @Test
  void nuevoJugadorTest() {
    DesiredCapabilities caps = new DesiredCapabilities();

    caps.setJavascriptEnabled(true);
    caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
    caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});

    driver = new PhantomJSDriver(caps);

    driver.navigate().to("http://localhost:8080/Baloncesto/");


    String nombreNuevoJug = "Mirotic";


    /* 1. Votación de nuevo jugador (otro) */
    driver.findElement(By.id("botonOtros")).click();
    driver.findElement(By.id("txtOtros")).sendKeys(nombreNuevoJug);
    driver.findElement(By.id("votar")).click();
    driver.findElement(By.id("returnButton")).click();


    /* 2. Pulsación: "Ver Votos" */
    driver.findElement(By.id("verVotos")).click();


    /* 3. Comprobación de que ese jugador tiene 1 voto */
    WebElement tabla = driver.findElement(By.id("tablaVotos"));
    ArrayList<WebElement> rows = new ArrayList<>(tabla.findElements(By.tagName("tr")));
    rows.remove(0);

    boolean expResult = true;
    boolean result = false;
    int j = 0;

    // Se comprueba para cada jugador que votos == 0
    while(!result && j < rows.size()) {
      List<WebElement> celdas = rows.get(j).findElements(By.tagName("td"));

      if (celdas.get(1).getText().trim().equals(nombreNuevoJug) && celdas.get(2).getText().trim().equals("1")) {
        result = true;
      }
      j++;
    }

    assertEquals(expResult, result, "  [!] No se ha votado al jugador nuevo");

    driver.close();
    driver.quit();
  }


}