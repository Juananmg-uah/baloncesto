import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;


class ModeloDatosTest {
  @Test
  void testActualizarJugadorMoquito() {
    System.out.println("-> PRUEBA: actualizarJugador con Moquito");

    ModeloDatos modelo = mock(ModeloDatos.class);
    doNothing().when(modelo).actualizarJugador(isA(String.class));

    modelo.actualizarJugador("");

    verify(modelo, times(1)).actualizarJugador("");

    System.out.printf("   [OK] El jugador se ha actualizado [mockito] \n\n");
  }


  @Test
  void testExisteJugador() {
    System.out.println("-> PRUEBA: existeJugador");

    String nombre = "Llull";
    ModeloDatos instance = new ModeloDatos();

    boolean expResult = true;
    boolean result = instance.existeJugador(nombre);
    assertEquals(expResult, result);

    System.out.printf("   [OK] El jugador existe: " + result + "\n\n");
  }


  @Test
  void testActualizarJugador() {
    System.out.println("-> PRUEBA: actualizarJugador");

    String nombre = "Llull";
    ModeloDatos instance = new ModeloDatos();

    /* Votos originales */
    int preResult = instance.recuperarVotosJugador(nombre);

    /* Votos actualizados */
    instance.actualizarJugador(nombre);
    int result = instance.recuperarVotosJugador(nombre);

    int expResult = 1;

    assertEquals(preResult, (result - 1));
    assertEquals(expResult, result);

    System.out.printf("   [OK] El jugador se ha actualizado. Votos: " + preResult + " -> " + result + "\n\n");
  }

  @Test
  void testPonerVotosACero() {
    System.out.println("\n\n-> PRUEBA: testVotosACero");

    ModeloDatos instance = new ModeloDatos();


    /* Comprobamos el Numero de filas */
    int expNumJug = 3;
    int numJug = instance.recuperarNumJugadores();
    assertEquals(expNumJug, numJug);


    /* Comprobamos que se generan datos */
    System.out.println("      - Generando datos de prueba");

    String votosString = instance.generarDatosPrueba(numJug);
    String[] votos = votosString.split(", ");

    for (String v: votos) {
      assertNotEquals("0", v);
    }

    System.out.println("      - Datos de prueba [" + votosString + "] generados");


    /* Comprobamos que se ponen los votos a 0 */
    System.out.println("      - Ponemos los votos a 0 de nuevo");

    String votosACeroString = instance.ponerVotosACero(numJug);


    String expResult = "0,0,0,";
    assertEquals(expResult, votosACeroString);

    System.out.println("   [OK] Datos [" + votosACeroString + "] actualizados");

  }
}