
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.ArrayList;
import java.util.Iterator;

import java.util.logging.Level;
import java.util.logging.Logger;


public class Acb extends HttpServlet {
  private ModeloDatos bd;

  private final Logger logger = Logger.getLogger("bitacora.subnivel.Control");
  private String errorMesgHeader = "     -> El error es: ";

  @Override
  public void init(ServletConfig cfg) throws ServletException {
    bd = new ModeloDatos();
    bd.abrirConexion();
  }

  @Override
  public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
    HttpSession s = req.getSession(true);
    String action = req.getParameter("action");


    if (action.equalsIgnoreCase("votar")) {
      String nombreP = req.getParameter("txtNombre");
      String nombre = req.getParameter("R1");
      if (nombre.equals("Otros")) {
          nombre = req.getParameter("txtOtros");
      }

      if (bd.existeJugador(nombre)) {
        bd.actualizarJugador(nombre);
      } else {
        bd.insertarJugador(nombre);
      }

      s.setAttribute("nombreCliente", nombreP);

      // Llamada a la página jsp que nos da las gracias
      try {
        res.sendRedirect(res.encodeRedirectURL("TablaVotos.jsp"));
      } catch (Exception e) {
        logger.log(Level. SEVERE, e.getMessage());
      }
    } else if (action.equalsIgnoreCase("votosACero")) {
      int numJug = bd.recuperarNumJugadores();
      bd.ponerVotosACero(numJug);

      // Llamada a la página de inicio de confirmación de votos a cero
      try {
        res.sendRedirect(res.encodeRedirectURL("VotosACero.jsp"));
      } catch (Exception e) {
        logger.log(Level. SEVERE, e.getMessage());
      }
    } else if (action.equalsIgnoreCase("verVotos")) {
      int numJug = bd.recuperarNumJugadores();
      ArrayList<String[]> listaVotos = bd.recuperarListaDeVotos(numJug);

      Iterator<String[]> listaIter = listaVotos.iterator();

      s.setAttribute("listaIter", listaIter);

      // Llamada a la página jsp que muestra los votos
      try {
        res.sendRedirect(res.encodeRedirectURL("VerVotos.jsp"));
      } catch (Exception e) {
        logger.log(Level. SEVERE, e.getMessage());
      }
    }
  }

  @Override
  public void destroy() {
    bd.cerrarConexion();
    super.destroy();
  }
}
