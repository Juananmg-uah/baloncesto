import java.sql.*;
import java.util.Random;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;


public class ModeloDatos {

  private Connection con;
  private Statement set;
  private ResultSet rs;

  private final Logger logger = Logger.getLogger("bitacora.subnivel.Control");

  private String errorMesgHeader = "     -> El error es: ";

  public void abrirConexion() {

    try {
      Class.forName("com.mysql.cj.jdbc.Driver");

      // Con variables de entorno
      String dbHost = System.getenv().get("DATABASE_HOST");
      String dbPort = System.getenv().get("DATABASE_PORT");
      String dbName = System.getenv().get("DATABASE_NAME");
      String dbUser = System.getenv().get("DATABASE_USER");
      String dbPass = System.getenv().get("DATABASE_PASS");

      String url = dbHost + ":" + dbPort + "/" + dbName;
      con = DriverManager.getConnection(url, dbUser, dbPass);

    } catch (Exception e) {
      // No se ha conectado
      logger.log(Level. SEVERE, " [!] No se ha podido conectar");
      logger.log(Level. SEVERE, errorMesgHeader + e.getMessage());
    }
  }

  public boolean existeJugador(String nombre) {
    boolean existe = false;
    String cad;
    try {
      abrirConexion();
      set = con.createStatement();

      rs = set.executeQuery("SELECT * FROM Jugadores");
      while (rs.next()) {
        cad = rs.getString("Nombre");
        cad = cad.trim();
        if (cad.compareTo(nombre.trim()) == 0) {
          existe = true;
        }
      }
      rs.close();
      set.close();

      logger.log(Level. INFO, "    * existeJugador finalizado con exito");

    } catch (Exception e) {
      // No lee de la tabla
      logger.log(Level. SEVERE, " [!] No lee de la tabla");
      logger.log(Level. SEVERE, errorMesgHeader + e.getMessage());
    }
    return (existe);
  }

  public void actualizarJugador(String nombre) {
    try {
      abrirConexion();
      set = con.createStatement();

      set.executeUpdate("UPDATE Jugadores SET votos=votos+1 WHERE nombre LIKE '" + nombre + "'");

      rs.close();
      set.close();

      logger.log(Level. INFO, "    * actualizarJugador finalizado con exito");

    } catch (Exception e) {
      // No modifica la tabla
      logger.log(Level. SEVERE, " [!] No modifica la tabla");
      logger.log(Level. SEVERE, errorMesgHeader + e.getMessage());
    }
  }

  public void insertarJugador(String nombre) {
      try {
        abrirConexion();
        set = con.createStatement();

        set.executeUpdate("INSERT INTO Jugadores " + " (nombre,votos) VALUES ('" + nombre + "',1)");

        rs.close();
        set.close();

        logger.log(Level. INFO, "    * insertarJugador finalizado con exito");

      } catch (Exception e) {
        // No inserta en la tabla
        logger.log(Level. SEVERE, " [!] No inserta en la tabla");
        logger.log(Level. SEVERE, errorMesgHeader + e.getMessage());
      }
  }



  /*                                             *
  *     INICIO - AÑADIDO NUEVA FUNCIONALIDAD     *
  *                                              */

  public ArrayList<String[]> recuperarListaDeVotos(int numJug) {
    ArrayList<String[]> listaVotos = new ArrayList<>();

    try {
      for(int i = 1; i <= numJug; i++) {
        String nombre = recuperarNombreJugador(Integer.toString(i));
        String votos = Integer.toString(recuperarVotosJugador(nombre));

        String cad[] = {nombre, votos};
        listaVotos.add(cad);
      }

      logger.log(Level. INFO, "    * recuperarListaDeVotos finalizado con exito");
    } catch (Exception e) {
      // No modifica la tabla
      logger.log(Level. SEVERE, " [!] No recuepera la lista de votos correctamente");
      logger.log(Level. SEVERE, errorMesgHeader + e.getMessage());
    }

    return listaVotos;
  }

  public String recuperarNombreJugador(String id) {
    String nombre = "-";

    try {
      abrirConexion();
      set = con.createStatement();
      rs = set.executeQuery("SELECT nombre FROM Jugadores WHERE id LIKE '" + id + "'");

      if (rs.next()) {
        nombre = rs.getString("nombre");
      }

      rs.close();
      set.close();

      logger.log(Level. INFO, "    * recuperarNombreJugador finalizado con exito");

    } catch (Exception e) {
      // No modifica la tabla
      logger.log(Level. SEVERE, " [!] No recuepera el nombre correctamente");
      logger.log(Level. SEVERE, errorMesgHeader + e.getMessage());
    }

    return nombre;
  }


  public int recuperarVotosJugador(String nombre) {
    int votos = 0;

    try {
      abrirConexion();
      set = con.createStatement();
      rs = set.executeQuery("SELECT votos FROM Jugadores WHERE nombre LIKE '" + nombre + "'");

      if (rs.next()) {
        votos = rs.getInt("votos");
      }

      rs.close();
      set.close();

      logger.log(Level. INFO, "    * recuperarVotosJugador finalizado con exito");

    } catch (Exception e) {
      // No modifica la tabla
      logger.log(Level. SEVERE, " [!] No recuepera el número de Votos correctamente");
      logger.log(Level. SEVERE, errorMesgHeader + e.getMessage());
    }

    return votos;
  }


  public int recuperarNumJugadores() {
    int numJugadores = 0;

    try {
      abrirConexion();
      set = con.createStatement();
      rs = set.executeQuery("SELECT COUNT(*) AS nJug FROM Jugadores");

      if (rs.next()) {
        numJugadores = rs.getInt("nJug");
      }

      rs.close();
      set.close();

      logger.log(Level. INFO, "    * recuperarNumJugadores finalizado con exito");

    } catch (Exception e) {
      // No modifica la tabla
      logger.log(Level. SEVERE, " [!] No recuepera el número de Jugadores correctamente");
      logger.log(Level. SEVERE, errorMesgHeader + e.getMessage());
    }

    return numJugadores;
  }

  public String generarDatosPrueba(int numJug) {
    String concatVotes = "";

    try {
      abrirConexion();
      set = con.createStatement();
      Random claseRandom = new Random();

      for (int i = 1; i <= numJug; i++) {
        int votosRand = 1 + claseRandom.nextInt(30);
        set.executeUpdate("UPDATE Jugadores SET votos=" + votosRand + " WHERE id LIKE '" + i + "'");
      }


      rs = set.executeQuery("SELECT votos FROM Jugadores");

      while(rs.next()){
        concatVotes = concatVotes + rs.getString("votos");
        concatVotes = concatVotes + ",";
      }

      rs.close();
      set.close();
    } catch (Exception e) {
      // No modifica la tabla
      logger.log(Level. SEVERE, " [!] No genera datos correctamente");
      logger.log(Level. SEVERE, errorMesgHeader + e.getMessage());
    }

    logger.log(Level. INFO, "    * generarDatosPrueba finalizado con exito");

    return(concatVotes);
  }

  public String ponerVotosACero(int numJug) {
    String concatVotes = "";

    try {
      abrirConexion();
      set = con.createStatement();
      set.executeUpdate("UPDATE Jugadores SET votos=0");

      for (int i = 1; i <= numJug; i++) {
        rs = set.executeQuery("SELECT votos FROM Jugadores WHERE id LIKE '" + i + "'");

        while(rs.next()){
          concatVotes = concatVotes + rs.getString("votos");
          concatVotes = concatVotes + ",";
        }
      }

      rs.close();
      set.close();

      logger.log(Level. INFO, "    * ponerVotosACero finalizado con exito");

    } catch (Exception e) {
      // No modifica la tabla
      logger.log(Level. SEVERE, " [!] No modifica los votos a cero");
      logger.log(Level. SEVERE, errorMesgHeader + e.getMessage());
    }

    return(concatVotes);
  }


  /*                                          *
  *     FIN - AÑADIDO NUEVA FUNCIONALIDAD     *
  *                                           */



  public void cerrarConexion() {
    try {
      con.close();
    } catch (Exception e) {
      logger.log(Level. SEVERE, " [!] No cierra la conexión");
      logger.log(Level. SEVERE, errorMesgHeader + e.getMessage());
    }
  }

}
