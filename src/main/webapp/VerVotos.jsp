
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>

<!DOCTYPE>
<html lang="es">
    <head>
      <title>Votaci&oacute;n mejor jugador liga ACB</title>
      <link href="estilos.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="resultado">
        <% Iterator<String[]> listaVotos = (Iterator<String[]>) session.getAttribute("listaIter"); %>

        <h1>Resultados de la votaci&oacute;n</h1>
        <hr>

        <h4>Resultados de la votaci&oacute;n</h4>

        <table id="tablaVotos" style="margin-left: auto; margin-right: auto;">
          <thead>
            <th scope="col"></th>
            <th scope="col">Nombre</th>
            <th scope="col">Votos</th>
          </thead>
          <tbody>

            <%
              int i = 0;
              while (listaVotos.hasNext()) {
                String jug[] = listaVotos.next();
                i++;%>

            <tr>
              <td id="jug<%=i%>"><%=i%></td>
              <td id="nombre<%=i%>"><%=jug[0]%></td>
              <td id="votos<%=i%>"><%=jug[1]%></td>
            </tr>

            <% } %>
          </tbody>
        </table>

        <br>
        <br> <a href="index.html" id="returnButton"> Ir al comienzo</a>
    </body>
</html>
